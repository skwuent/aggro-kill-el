;;; shell-from-here.el --- shell at the location of current buffer

;; Copyright (C) 2016 NGK Sternhagen

;; Author: NGK Sternhagen <sternhagen@protonmail.ch>
;; Version: 0.0.0
;; Keywords: shell
;; URL: https://gitlab.com/skwuent/shell-from-here

;;; Commentary:

;;;###autoload
(defun aggro-kill ()
  (interactive)
  (let ((my-buffer (current-buffer)))
    (if (get-buffer-process my-buffer)
        (set-process-query-on-exit-flag (get-buffer-process my-buffer) nil))
    (message (concat  "aggro-kill killing buffer " (buffer-name (current-buffer)) "!"))
    (kill-buffer my-buffer)))
